#include <iostream>
#include <fstream>

const int size = 10;

using namespace std;
void readFile(int i, int arr[size]);
void sortAccordingToindexes(int[],int[],int[],int[],int[],int[]);

int main()
{


	int judge_one[size];
	int judge_two[size];
	int judge_three[size];
	int judge_four[size];
	int judge_five[size];
	int judge_add[size];
	int indexes[size];

	readFile(1, judge_one);
	readFile(2, judge_two);
	readFile(3, judge_three);
	readFile(4, judge_four);
	readFile(5, judge_five);
	readFile(6, indexes);

	sortAccordingToindexes(judge_one,judge_two,judge_three,judge_four,judge_five,indexes);
	return 0;
}

void readFile(int i, int arr[size])
{
	ifstream file;

	if (i == 1) {file.open("../../data/source1.txt");}
	else if(i == 2) {file.open("../../data/source2.txt");}
	else if(i == 3) {file.open("../../data/source3.txt");}
	else if(i == 4) {file.open("../../data/source4.txt");}
	else if (i == 5) {file.open("../../data/source5.txt");}
	else {file.open("../../data/sorted_indexes.txt");}


	for(int i=0; i < size; i++)
	{
		file >> arr[i];
	}
}

void sortAccordingToindexes(int arr1[],int arr2[],int arr3[],int arr4[],int arr5[], int indexes[])
{
	fstream file1;
	fstream file2;
	fstream file3;
	fstream file4;
	fstream file5;


	file1.open("../../data/source1_count.txt", ios::out);
	file2.open("../../data/source2_count.txt", ios::out);
	file3.open("../../data/source3_count.txt", ios::out);
	file4.open("../../data/source4_count.txt", ios::out);
	file5.open("../../data/source5_count.txt", ios::out);
	
	for (int i = 0; i < size; i++)
	{
		int y = 0;
		int z = 0;
		int x = indexes[i];
		y = arr1[0];
		z = arr1[x];
		file1 << arr1[x] <<"\n";
		file2 << arr2[x] <<"\n";
		file3 << arr3[x]<<"\n";
		file4 << arr4[x] <<"\n";
		file5 << arr5[x] <<"\n";
	}
	
}