#include <iostream>
#include <fstream>
#include <string>

const int size = 10;

//element contains the value and its respective index to keep track of orignal index after sorting.
class element
{
public:
	int value;
	int index;

	element()
	{
		value = 0;
		index = 0;
	}
};



using namespace std;


void readFile(int i, element* arr[size]);
void addArrays(element*[size],element*[size],element*[size],element*[size],element*[size],element*[size]);
void radixsort(element* arr[], int n);
void countSort(element* arr[],int n, int exp);
element* getMax(element* arr[], int n);
void export_to_file(fstream&,fstream&, element* arr[]);



int main()
{
	ifstream file;
	fstream output;
	fstream output_indexes;
	element* arr[size];


	element* judge_one[size];
	element* judge_two[size];
	element* judge_three[size];
	element* judge_four[size];
	element* judge_five[size];
	element* judge_add[size];

	readFile(1, judge_one);
	readFile(2, judge_two);
	readFile(3, judge_three);
	readFile(4, judge_four);
	readFile(5, judge_five);
	  
	addArrays(judge_one,judge_two,judge_three,judge_four,judge_five,judge_add);

	radixsort(judge_add, size);

	export_to_file(output,output_indexes, judge_add);
	
	return 0;
}

//store all the sources in arrays
void readFile(int i, element* arr[size])
{
	ifstream file;


	if	   (i == 1) {file.open("../../data/source1.txt");}
	else if(i == 2) {file.open("../../data/source2.txt");}
	else if(i == 3) {file.open("../../data/source3.txt");}
	else if(i == 4) {file.open("../../data/source4.txt");}
	else {file.open("../../data/source5.txt");}


	for(int i=0; i < size; i++)
	{
		element* n = new element;
		file >> n->value;
		n->index = i;
		arr[i] = n;
	}
}



//This function adds all the arrays by respective index
void addArrays(element* judge_one[size],element* judge_two[size],element* judge_three[size],element* judge_four[size],element* judge_five[size], element* judge_add[size])
{
	for ( int i = 0; i < size;  i++)
	{
		element* n = new element;
		n->value = judge_one[i]->value + judge_two[i]->value + judge_three[i]->value + judge_four[i]->value + judge_five[i]->value;
		n->index = i;
		judge_add[i] = n;
	}
}


//main radix sort function
void radixsort(element* arr[], int n)
{
	element* m = getMax(arr, n);
	for (int exp = 1; m->value / exp > 0; exp *= 10)
	{
		countSort(arr, n, exp);
	}
}



//this function constructs the buckets continously as the loop iterates
void countSort(element* arr[],int n, int exp)
{
	element* output[size];
	int i, count[10] = { 0 };
	for (i = 0; i < n; i++)
	{
		count[(arr[i]->value / exp) % 10]++;
	}
		
	for (i = 1; i < 10; i++)
	{
		count[i] += count[i - 1];
	}
		
	for (i = n - 1; i >= 0; i--)
	{
		output[count[(arr[i]->value / exp) % 10] - 1] = arr[i];
		count[(arr[i]->value / exp) % 10]--;
	}

	for (i = 0; i < n; i++)
	{
		arr[i] = output[i];
	}
		
}


element* getMax(element* arr[], int n)
{
	element* max = new element;
	max->value = arr[0]->value;
	for (int i = 1; i < n; i++)
	{
		if (arr[i]->value > max->value)
			max = arr[i];
	}
	return max;
}


//exports the final outcome to a file in the folder
void export_to_file(fstream& file1,fstream& file2, element* arr[])
{
	
	file1.open("../../data/sorted_sources.txt", ios::out);
	file2.open("../../data/sorted_indexes.txt", ios::out);

	
	for (int i = 0; i < size; i++)
	{
		file1 << arr[i]->value <<"\n";
		file2 << arr[i]->index <<"\n";
	}
}

