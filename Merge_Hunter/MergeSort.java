import java.util.Scanner;
import java.io.*;


public class MergeSort {
	
public static void main(String[] args) throws IOException{
	
		int num_inversions;			//variable to hold number of inversions
		double elapsedTime = 0;
    
		int A[] = new int [10000];			//Array to hold values from source1
		System.out.println("source1: ");
		populateA(A);
		long startTime = System.nanoTime(); 
		num_inversions = inv_count(A);			//sets num_inversions equal to result of inv_count method
		long endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);	
		A = merge_sort(A);				//sorts the values assigned to A
		//print(A);
		
		
		int B[] = new int [10000];			//Array to hold values from source2
		System.out.println("\n\nSource2: ");
		populateB(B);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(B);
		 endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		B = merge_sort(B);
		//print(B);
		
		
		int C[] = new int [10000];			//Array to hold values from source3
		System.out.println("\n\nSource3: ");
		populateC(C);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(C);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		C = merge_sort(C);
		//print(C);
		
		
		int D[] = new int [10000];			//Array to hold values from source4
		System.out.println("\n\nSource4: ");
		populateD(D);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(D);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		D = merge_sort(D);
		//print(D);
		
		
		int E[] = new int [10000];			//Array to hold values from source5
		System.out.println("\n\nSource5: ");
		populateE(E);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(E);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		E = merge_sort(E);
		//print(E);
		
		
		System.out.println("\nTotal elapsed time: " + elapsedTime/1000000000 + " seconds");
	}



public static int[] merge_sort(int[] F){
	if(F.length <= 1){			//Base case; if there are 0 or 1 elements, array is sorted
		return F;
	}
	
	int midpoint = F.length/2;			//sets midpoint to the middle value
	int[] left = new int[midpoint];		//creates an array to hold left-hand values
	int[] right;						//creates an array to hold right-hand values
	
	if(F.length%2 == 0){
		right = new int[midpoint];		//If even number of elements, array can be divided evenly
	}
	else{
		right = new int[midpoint+1];	//If odd number of elements, right side starts 1 element after midpoint
	}
	
	int[] result = new int[F.length];	//variable to hold the result of the array
	
	for(int i = 0; i < midpoint; i++){	//fills left-hand array
		left[i] = F[i];
		
	}
	
	int x=0;
	for(int j = midpoint; j < F.length; j++){		//fills right-hand array
		if(x<right.length){
		right[x] = F[j];
		x++;
		}
	}
	
	
	left = merge_sort(left);		//recursive call to split array
	right = merge_sort(right);		//recursive call to split array
	
	result = merge(left, right);	//merges left-hand and right-hand sides
	
	
	return result;
	
}

public static int[] merge(int[] left, int[] right){
	int lengthResult = left.length + right.length;		//gets length of total list
	int[] result = new int[lengthResult];				//set result to size of list
	int indexL = 0;										//index to traverse left-hand array
	int indexR = 0;										//index to traverse right-hand array
	int indexRes = 0;
	
	while(indexL < left.length || indexR < right.length){
		if(indexL < left.length && indexR < right.length){		//checks for end of list
			if(left[indexL] <= right[indexR]){					//checks for end of left-hand list
				result [indexRes] = left[indexL];
				indexL++;
				indexRes++;
			}
			
			else{
				result[indexRes] = right[indexR];		//checks for end of right-hand list
				indexR++;
				indexRes++;
			}
		}
		
		else if(indexL < left.length){
			result[indexRes] = left[indexL];
			indexL++;
			indexRes++;
		}
		
		else if(indexR < right.length){
			result[indexRes] = right[indexR];
			indexR++;
			indexRes++;
		}
	}
	return result;
}

public static int inv_count(int[] F){
	
	int num_inversions = 0;
	  for (int i = 0; i < F.length - 1; i++)	//traverses array
	    for (int j = i+1; j < F.length; j++)	//traverses array 1 element after i
	      if (F[i] > F[j]){						//compares F[i] and F[j] to find inversions
	    	  if(F[i] > 0 && F[j] > 0)     //only for testing purposes
	    		  num_inversions++;
	      }
	
	return num_inversions;
}

public static void print(int[] F){				//prints sorted list
	for(int i = 0; i < F.length; i++){
		if(F[i] > 0){
		System.out.print(F[i] + " ");
		}
	}
}

public static int[] populateA(int[] F)throws IOException{		//loads data from Source1 into A
	
	int j = 0;
	File inFile = new File ("../data/source1_count.txt");

    Scanner sc = new Scanner (inFile);
    while (sc.hasNextLine())
    {
      String line = sc.nextLine();
      int i = Integer.parseInt(line);
      //System.out.println (line);
      F[j] = i;
      j++;
    }
    sc.close();
	return F;
}

public static int[] populateB(int[] F)throws IOException{		//loads data from Source2 into B
	
	int j = 0;
	File inFile = new File ("../data/source2_count.txt");

    Scanner sc = new Scanner (inFile);
    while (sc.hasNextLine())
    {
      String line = sc.nextLine();
      int i = Integer.parseInt(line);
      //System.out.println (line);
      F[j] = i;
      j++;
    }
    sc.close();
	return F;
}

public static int[] populateC(int[] F)throws IOException{		//loads data from Source3 into C
	
	int j = 0;
	File inFile = new File ("../data/source3_count.txt");

    Scanner sc = new Scanner (inFile);
    while (sc.hasNextLine())
    {
      String line = sc.nextLine();
      int i = Integer.parseInt(line);
      //System.out.println (line);
      F[j] = i;
      j++;
    }
    sc.close();
	return F;
}

public static int[] populateD(int[] F)throws IOException{		//loads data from Source4 into D
	
	int j = 0;
	File inFile = new File ("../data/source4_count.txt");

    Scanner sc = new Scanner (inFile);
    while (sc.hasNextLine())
    {
      String line = sc.nextLine();
      int i = Integer.parseInt(line);
      //System.out.println (line);
      F[j] = i;
      j++;
    }
    sc.close();
	return F;
}

public static int[] populateE(int[] F)throws IOException{		//loads data from Source5 into E
	
	int j = 0;
	File inFile = new File ("../data/source5_count.txt");

    Scanner sc = new Scanner (inFile);
    while (sc.hasNextLine())
    {
      String line = sc.nextLine();
      int i = Integer.parseInt(line);
      //System.out.println (line);
      F[j] = i;
      j++;
    }
    sc.close();
	return F;
}
}