﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quicksort
{
    public static class qs_recursive
    {
        public static int invs = 0;
        public static void Sort(int[] arr, int start, int finish)
        {
            int left = start;
            int right = finish;
            int elem = arr[(start + finish) / 2];
            int temp;
            while (left <= right)
            {
                while (arr[left] < elem)
                {
                    left++;
                }
                while (arr[right] > elem)
                {
                    right--;
                }
                if (left <= right)
                {
                    temp   = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                    left++;
                    right--;

                    invs += right - left;
                }
            }
            if (left < finish)
            {
                Sort(arr, left, finish);
            }
            if (right > start)
            {
                Sort(arr, start, right);
            }
        }



    }
}
