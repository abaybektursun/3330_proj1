﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.IO;

namespace quicksort
{
    public class Work
    {
        // Debug
        public static Random rand    = new Random();
        // Debug

        public static int inversions = 0;
        public static void Main()
        {
            Console.WriteLine("Quick Sort Inversion Counting:");
            perform("source1_count.txt");
            Console.WriteLine("Recursive QSnlog(n):" + performRecursive("source1_count.txt") + "\n");
            inversions = 0;
            perform("source2_count.txt");
            Console.WriteLine("Recursive QSnlog(n):" + performRecursive("source2_count.txt") + "\n");
            inversions = 0;
            perform("source3_count.txt");
            Console.WriteLine("Recursive QSnlog(n):" + performRecursive("source3_count.txt") + "\n");
            inversions = 0;
            perform("source4_count.txt");
            Console.WriteLine("Recursive QSnlog(n):" + performRecursive("source4_count.txt") + "\n");
            inversions = 0;
            perform("source5_count.txt");
            Console.WriteLine("Recursive QS nlog(n):" + performRecursive("source5_count.txt") + "\n");
        }
        // It is not recursive for efficiency purposes
		// Cannot count inversions properly
        public static int[] qsAction(int[] arr)
        {
            int i = 0, left, right;
            int[] beg;
            int[] end;

            int arrSize = arr.Length; 
            int pivot;

            beg = new int[arr.Length];
            end = new int[arr.Length];
            beg[0] = 0;
            end[0] = arrSize;
            while (i >= 0)
            {
                // Left pointer
                left = beg[i];
                // Right pointer
                right = end[i] - 1;
                if (left < right)
                {
                    // Left most element is pivot
                    pivot = arr[left];
                    
                    while (left < right)
                    {
                        // Move the right pointer to the left until larger than pivot element os found
                        while (arr[right] >= pivot && left < right) right--;
                        if (left < right && arr[left] > arr[right])
                        {
                            arr[left] = arr[right];
                            left++;
                            inversions += right - (left - 1);
                        }
                        // Move the left pointer to the right until smaller than pivot element is found
                        while (arr[left] <= pivot && left < right) left++;
                        if (left < right && arr[left] > arr[right])
                        {
                            arr[right] = arr[left];
                            right--;
                        }
                    }
                    arr[left]  = pivot; 
                    beg[i + 1] = left + 1; 
                    end[i + 1] = end[i]; 
                    end[i]     = left;
                    i++;
                }
                else
                {
                    i--;
                }
            }
            return arr;
        }
		public static void perform(string fileN)
		{
			int[] result;
            // Read File
            string iam_tired = System.IO.Directory.GetCurrentDirectory();
            System.IO.DirectoryInfo directoryInfo = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(iam_tired).FullName).FullName);
            string fielPath = directoryInfo.FullName + @"\data\" + fileN;
            string[] fileLines = System.IO.File.ReadAllLines(fielPath);
            // String array to int array
            int[] fileInts = fileLines.Select(int.Parse).ToArray();
            // Sort
            int[] sample = new int[] { 2, 3, 4, 1, 5, 6, 7, 8, 9 };
            var fileList = File.ReadAllLines(fielPath);
            List<string> sourceList = new List<string>(fileList);

            //Create an array out of the List
            int[] source = new int[sourceList.Count];
            int[] source_copy = new int[sourceList.Count];

            int i = 0;
            foreach(string a_num in sourceList)
            {
                source[i]      = Int32.Parse(a_num);
                source_copy[i] = Int32.Parse(a_num);
                i++;
            }

            result = qsAction(source);
            foreach (int a_num in result)
            {
                //System.Console.WriteLine(a_num);
            }
            System.Console.WriteLine(fileN + " Approximate Inversions nlog(n): " + inversions);

            int n = source_copy.Length;
            int inv_count = 0;
            for (int h = 0; h < n - 1; h++)
            {
                for (int j = h + 1; j < n; j++)
                {
                    if (source_copy[h] > source_copy[j])
                        inv_count++;
                }
            }
            System.Console.WriteLine(fileN + " Actual Inversions n^2: " + inv_count);
		}

        public static int performRecursive(string fileN)
        {
            qs_recursive.invs = 0;
            //inversions
            //int invs = 0;
            // Read File
            string iam_tired = System.IO.Directory.GetCurrentDirectory();
            System.IO.DirectoryInfo directoryInfo = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(iam_tired).FullName).FullName);
            string fielPath = directoryInfo.FullName + @"\data\" + fileN;
            var fileList = File.ReadAllLines(fielPath);
            List<string> sourceList = new List<string>(fileList);
            //Create an array out of the List
            int[] source = new int[sourceList.Count];

            int k = 0;
            foreach (string a_num in sourceList)
            {
                source[k] = Int32.Parse(a_num);
                k++;
            }
            // Count inversions
            qs_recursive.Sort(source, 0, source.Length - 1);

            return qs_recursive.invs;
        }
        
    }
}
